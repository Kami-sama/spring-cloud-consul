package com.test.consul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.actuate.autoconfigure.EndpointWebMvcAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.ManagementServerPropertiesAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.MetricExportAutoConfiguration;
import org.springframework.boot.autoconfigure.context.PropertyPlaceholderAutoConfiguration;
import org.springframework.boot.autoconfigure.web.DispatcherServletAutoConfiguration;
import org.springframework.boot.autoconfigure.web.EmbeddedServletContainerAutoConfiguration;
import org.springframework.cloud.autoconfigure.RefreshAutoConfiguration;
import org.springframework.cloud.autoconfigure.RefreshEndpointAutoConfiguration;
import org.springframework.cloud.consul.ConsulAutoConfiguration;
import org.springframework.cloud.consul.config.ConsulConfigAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Hello world!
 */
@SpringBootConfiguration
@ComponentScan("com.test.consul.controller")
@EnableWebMvc
@PropertySource({"classpath:bootstrap.properties", "classpath:application.properties"})
@Import({
        ConsulAutoConfiguration.class,
        ConsulConfigAutoConfiguration.class,
        RefreshAutoConfiguration.class,
        RefreshEndpointAutoConfiguration.class,
        MetricExportAutoConfiguration.class,//IMPORTANT FOR REFRESH PROPERTIES, WHY? DONT KNOW yet

//Spring boot conf
        DispatcherServletAutoConfiguration.class,
        EmbeddedServletContainerAutoConfiguration.class,
        EndpointWebMvcAutoConfiguration.class,
        PropertyPlaceholderAutoConfiguration.class,
        ManagementServerPropertiesAutoConfiguration.class,

/**
 //TODO Possible needed for others consul services

 //        RibbonConsulAutoConfiguration.class,
 //        RibbonAutoConfiguration.class,
 //        SimpleDiscoveryClientAutoConfiguration.class,
 //        UtilAutoConfiguration.class,

 //        ChannelBindingAutoConfiguration.class,
 //        ChannelsEndpointAutoConfiguration.class,
 //        ConfigurationPropertiesRebinderAutoConfiguration.class,
 //        ConsulServiceRegistryAutoConfiguration.class,
 //        CommonsClientAutoConfiguration.class,
 //        JacksonAutoConfiguration.class,
 //        JmxAutoConfiguration.class,
 //        LifecycleMvcEndpointAutoConfiguration.class,
 //        LoadBalancerAutoConfiguration.class,
 */

//        EndpointAutoConfiguration.class,
//        EndpointMBeanExportAutoConfiguration.class,
//        EndpointWebMvcManagementContextConfiguration.class,
//        ArchaiusAutoConfiguration.class,
//        AsyncLoadBalancerAutoConfiguration.class,
//        AuditAutoConfiguration.class,
//        BinderFactoryConfiguration.class,
//        BindingServiceConfiguration.class,
//        BusAutoConfiguration.class,
//        BusJacksonAutoConfiguration.class,
//        ErrorMvcAutoConfiguration.class,
//        GsonAutoConfiguration.class,
//        HealthIndicatorAutoConfiguration.class,
//        HttpEncodingAutoConfiguration.class,
//        HttpMessageConvertersAutoConfiguration.class,
//        HystrixAutoConfiguration.class,
//        InfoContributorAutoConfiguration.class,
//        MetricFilterAutoConfiguration.class,
//        MetricsInterceptorConfiguration.class,
//        MultipartAutoConfiguration.class,
//        PersistenceExceptionTranslationAutoConfiguration.class,
//        PersistenceExceptionTranslationAutoConfiguration.class,
//        PublicMetricsAutoConfiguration.class,
//        RxJavaAutoConfiguration.class,
//        ServerPropertiesAutoConfiguration.class,
//        ServiceRegistryAutoConfiguration.class,
//        ServoMetricsAutoConfiguration.class,
//        SpringApplicationAdminJmxAutoConfiguration.class,
//        JmxAutoConfiguration.class,
//        TraceRepositoryAutoConfiguration.class,
//        TraceWebFilterAutoConfiguration.class,
//        TransactionAutoConfiguration.class,
//        ValidationAutoConfiguration.class,
//        WebClientAutoConfiguration.class,
//        WebSocketAutoConfiguration.class,
//        GsonHttpMessageConvertersConfiguration.class,
//        GenericCacheConfiguration.class,
//        JacksonHttpMessageConvertersConfiguration.class,
//        NoOpCacheConfiguration.class,
//        RedisCacheConfiguration.class,
//        SimpleCacheConfiguration.class,
})
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

}
