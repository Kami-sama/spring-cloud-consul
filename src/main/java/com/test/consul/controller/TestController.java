package com.test.consul.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class TestController {

    @Autowired
    private Environment environment;

    @Value("${foo:inknown}")
    private String foo;

    @RequestMapping(value = "/")
    public String test() {
        String lol = environment.getProperty("Lol");
        System.out.println(lol);
        System.out.println(foo);
        return foo;
    }

    @RequestMapping(value = "/", params = "s")
    public String test1(String s){
        String lol = environment.getProperty(s);
        System.out.println(lol);
        return lol;
    }
}
